import React from "react";
import { Navigate, Outlet } from "react-router-dom";
import './RootLayout.css';
import Body from "~/components/Component/BodySection/Body";
import SideBar from '~/components/Component/SideBarSection/SideBar'
import { useAuth } from '~/context/AuthContext';
import {  useState,useEffect } from 'react';
import { FormModal } from '~/components/Component/CallingModal';
import { CallToUser } from '~/components/Component/CallToUser';
import { useSip } from '~/context/SipContext';
export function RootLayout() {
  const { user} = useAuth();
  const [openFormCall, setOpenFormCall] = useState(false);
  const [phonenumber, setPhoneNumber] = useState(null);
  const [openFormCalls, setOpenFormCalls] = useState(false);
  const { deviceclv } = useSip();

  function setStatus(){
    setOpenFormCall(true);

  }
  function handleOffCallToUser(
    number) {
    // deviceclv?.current.initiateCall(number);
    setOpenFormCalls(false);
  }
  function setOffStatus(){
    setOpenFormCall(false);
    setOpenFormCalls(true);
  }

  if (deviceclv?.current) {
    deviceclv.current.on('invite', (data) => {
      setPhoneNumber(Object.values(data)[1]);
      const timer = setTimeout(() => {
        // Thực hiện các thao tác cần thiết sau 2 giây
        setStatus(); // Giả sử setStatus là hàm bạn muốn gọi
      }, 1500);
    });
  }
  function handleOnClick(){
    console.log("Da nhan nghe duoc dth");
    setOffStatus();
    deviceclv.current.accept();
  }


  return (
    <>
      {openFormCall ? (
      <FormModal
      phone={phonenumber}
      handleOnClick={handleOnClick}
      setOffStatus={setOffStatus}
    />) : null
    }

{openFormCalls ? (
        <CallToUser
        handleOffCallToUser={handleOffCallToUser}
          setOpenFormCall={setOpenFormCalls}
          idUser='1'
          phone={phonenumber}
        />
      ) : null}
      <div className="container2">
        <SideBar />
        <div className="mainContent2">
          <Body />
          <div className="bottom2 flex">
            <Outlet />
          </div>
        </div>
      </div>
    </>
  );
}
