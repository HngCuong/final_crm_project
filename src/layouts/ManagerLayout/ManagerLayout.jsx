import React from "react";
import { Navigate,Outlet } from "react-router-dom";
import './RootLayout.css';
import Body from "~/components/Component/BodySection/Body";;
import SideBarManagerSection from '~/components/Component/SideBarManagerSection/SideBar'
import { useAuth } from '~/context/AuthContext';
export function ManagerLayout() {
  const { user} = useAuth();
  if (user === null) return <Navigate to='/login' />;
  console.log(user);
  return (
    <div className="container2">
      <SideBarManagerSection />
      <div className="mainContent2">
        <Body />
        <div className="bottom2">
          <Outlet />
        </div>
        </div>
    </div>
  );
}
