// Styles
import styles from './EditCampaignAdm.module.css';
import {
  Box, Typography
} from "@mui/material";
import { AiOutlineClose } from 'react-icons/ai';
// Components
import { Input } from '~/components/Material/Input';
import { Button } from '~/components/Material/Button';
import { useQuery, useMutation } from '@tanstack/react-query';
// Modules
import { motion } from 'framer-motion';
import { useAxios } from '~/context/AxiosContex';
//functions
import { useState } from 'react';
import { useEffect } from 'react';
import { toast } from 'react-hot-toast';
import { useRef } from 'react';
export default function EditCampaignAdm({ email,idBrand, setEditModal, refetch }) {
  const { updateTicket, getTicketById, getUserByStatus, getTicketIdById,postMail } = useAxios();
  const ods = useRef();
  const [branchName, setBrandName] = useState('');
  const postMailMutation = useMutation({
    mutationFn: (data) => {
      return postMail(data).then((res) => {
        console.log(res);
        return res;
      });
    },
  });
  //* Hàm tạo Ticket
  const handlePostMail = (e) => {
  console.log(email);
  const data = {
    subject: "Xác nhận đóng Ticket",
    body: "Ticket đã hoàn thành, Nếu cần hỗ trợ thêm xin gọi lại",
    toAddresses:[email]
  };
  postMailMutation.mutate({
    data
  });
};
  const {
    data: source,
  } = useQuery(
    {
      queryKey: ['source'],
      queryFn: async () => await getUserByStatus(),
    }
  );
  const {
    data: getBrandId,
  } = useQuery(
    {
      queryKey: ['getBrandId'],
      queryFn: async () => await getTicketIdById(idBrand),
    }
  );
  //* update brand mutations
  const updateBrandMutation = useMutation({
    mutationFn: (data) => {
      console.log(data);
      return updateTicket(data).then((res) => {

        return res;
      });
    },
    onSuccess: (data) => {
      console.log(data);
      if (data == '204') {
        handlePostMail();
        toast('Update thành công', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
        refetch();
        setEditModal(false);
      }
    },
  });
  function handleUpdateBrand(e) {
    e.preventDefault();
    console.log(getBrandId);
    const dataPatch = {
      id: getBrandId.id,
      branchId: getBrandId.branchId,
      note: getBrandId.note,
      customerId: getBrandId.customerId,
      ticketTypeId: getBrandId.ticketTypeId,
      levelId: getBrandId.levelId,
      ticketStatusId: 4,
      createdDate: getBrandId.createdDate,
      createdBy: getBrandId.createdBy,
      assignedUserId:  getBrandId.assignedUserId,
      code: getBrandId.code,
      title: getBrandId.title,
    };
  
    updateBrandMutation.mutate({ data: dataPatch });
  }
  return (
    <motion.div
      initial={{ opacity: 0, transition: 0.5 }}
      animate={{ opacity: 1, transition: 0.5 }}
      transition={{ type: 'spring' }}
      className={styles.formmoal}
    >
      <form>
        <div className={styles.head}>
          <h6>Bạn có muốn xác nhận kết thúc</h6>
          <AiOutlineClose
            onClick={() => setEditModal(false)}
            style={{ color: 'grey', fontSize: '20px', cursor: 'pointer' }}
          />
        </div>
        <div className={styles.buttons}>
            <Button
              className={styles.cancelbtn}
              onClick={(e) => {
                e.preventDefault();
                setEditModal(false);
              }}
            >
              HỦY
            </Button>
            <Button
              onClick={handleUpdateBrand}
              className={styles.addbtn}
            >
              Đồng Ý
            </Button>
          </div>
   
    
 
      </form>
    </motion.div>
  );
}
