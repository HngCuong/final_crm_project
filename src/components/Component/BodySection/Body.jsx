import React from 'react'
import './body.css'
import { Menu, MenuItem, IconButton } from '@mui/material';
import LogoutIcon from '@mui/icons-material/Logout';
import { useEffect, useMemo, useState, useRef, useCallback } from 'react';
import { BiSearchAlt } from 'react-icons/bi'
import { TbMessageCircle } from 'react-icons/tb'
import { MdOutlineNotificationsNone } from 'react-icons/md'
import { BsArrowRightShort } from 'react-icons/bs'
import { useAuth } from '~/context/AuthContext';
import { useNavigate } from 'react-router-dom';
import img from '~/assets/profile.jpeg'
import img2 from '~/assets/lg.png'
import video from '~/assets/fish.mp4'
import Clock from '~/components/Component/Clock/Clock';

const Body = () => {
  const [anchorEl, setAnchorEl] = useState(null);
  const { user, removeUser, removeToken } = useAuth();
  console.log(user);
  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };
  const navigate = useNavigate();
  const handleLogout = () => {
    removeUser();
    removeToken();
    navigate('/login');
  };
  return (
    <div className='topSection1'>
      <div className="headerSection flex">
        <div className="title">
        <h1>Chào mừng đến với {Object.values(user)[5]}</h1>
          <p>Hello {Object.values(user)[1]}, Chào mừng trở lại!!!</p>
        </div>

        <div className="searchBar flex">
          <input type='text' placeholder='Search' />
          <BiSearchAlt className='icon' />
        </div>

        <div className="adminDiv flex">
          <TbMessageCircle className='icon' />
          <MdOutlineNotificationsNone className='icon' />
          <div className='adminImage'>
            <img src={img} alt='Admin Image' onClick={handleMenuOpen}/>
          </div>
          <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleMenuClose}
      >
        <MenuItem onClick={handleLogout}>
          <LogoutIcon sx={{ marginRight: 1 }} />
          Logout
        </MenuItem>
      </Menu>
        </div>
      </div>

      <div className='cardSection flex'>
        
        <div className="rightCard flex">
          <div className="videoDiv">
            <video src={video} autoPlay loop muted></video>
          </div>
        </div>

        <div className="leftCard flex">
        <div className="main responsive ">
        <div className="relative z-10">
         <Clock/>
</div>
</div>

        </div>
      </div>
    </div>
  )
}

export default Body