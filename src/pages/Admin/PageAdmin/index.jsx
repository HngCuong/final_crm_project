import React from 'react'
import Breadcrumb from "~/components/Material/Breadcrumb/Breadcrumb";
import SimpleCard from "~/components/Material/SimpleCard/SimpleCard";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { useAxios } from '~/context/AxiosContex';
import { useQuery } from '@tanstack/react-query';
import { useState } from 'react';
import { useEffect } from 'react';
import { AddCampaignAdm } from '~/components/Addition/AddCampaignAdm';
import { EditCampaignAdm } from '~/components/Edition/EditBrand';
import { Loading } from '~/components/LoadingPage/Loading';
import { toast } from 'react-hot-toast';
import { useMutation } from '@tanstack/react-query';
import {
  Modal,
  Typography,
  Button,
  Box,
  Icon,
  Table,
  styled,
  TableRow,
  TableBody,
  TableCell,
  TableHead,
  IconButton,
  ImageList ,
  ImageListItem 
} from "@mui/material";
export default function AdminManager() {
  // STYLED COMPONENT
  const StyledTable = styled(Table)(({ theme }) => ({
    whiteSpace: "pre",
    "& thead": {
      "& tr": { "& th": { paddingLeft: 0, paddingRight: 0 } }
    },
    "& tbody": {
      "& tr": { "& td": { paddingLeft: 0, textTransform: "capitalize" } }
    }
  }));
  const Container = styled("div")(({ theme }) => ({
    margin: "00px",
    [theme.breakpoints.down("sm")]: { margin: "16px" },
    "& .breadcrumb": {
      marginBottom: "30px",
      [theme.breakpoints.down("sm")]: { marginBottom: "16px" }
    }
  }));
  const [brandData, setBrandData] = useState([]);
  const { getBrand, deleteBranchById } = useAxios();
  const [openModal, setOpenModal] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const [idBrand, setIdBrand] = useState();
  const [brandname, setBrandName] = useState();
  const [open, setOpen] = React.useState(false);
  const handleOpen = (name,id) =>{
  setBrandName(name);
  setIdBrand(id);
  setOpen(true);
  }
  const handleClose = () => setOpen(false);
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
  };
  
  const deleteBrand = useMutation({
    mutationFn: (data) => {
      return deleteBranchById(data).then((res) => {
        return res;
      });
    },
    onSuccess(data) {
      console.log(data);
      if (data == '') {
        toast('Xoá thành công', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
        refetch();
        handleClose();
      }
    },
  });
  function handleDelete() {
    deleteBrand.mutate(idBrand);
  }


  function handleEdit(id) {
    setIdBrand(id);
    setEditModal(true);
  }
  const {
    data: brand,
    isLoading,
    refetch,
  } = useQuery(
    {
      queryKey: ['brand'],
      queryFn: async () => await getBrand(),
    }
  );
  useEffect(() => {
    if (brand) {
      setBrandData(brand);
      console.log(brand);
    }
  }, [brand]);

 
 
  // async function handleOnClick(id) {
  //   function handleDelete() {
  //     deleteBrand.mutate(id);
  //   }
  //   await confirm(handleDelete);
  // }

  const itemData = [
    {
      img: 'https://firebasestorage.googleapis.com/v0/b/crmcustomer-3d184.appspot.com/o/images%2F1000004079.jpg?alt=media&token=a520c258-5f5e-4244-9e9a-e34883a70f62',
      title: 'Breakfast',
    },
 
  ];
  return (
    <Container>
      {openModal ? (
        <AddCampaignAdm
          refetch={refetch}
          setOpenModal={setOpenModal}
        />
      ) : null}
      {editModal ? (
        <EditCampaignAdm
          refetch={refetch}
          idBrand={idBrand}
          setEditModal={setEditModal}
        />
      ) : null}
      {isLoading ? (
        <div>
          <Loading
            size='90'
            color='#fc3b56'
          />
        </div>
      ) : (
        <>
          <div>
            <Modal
              open={open}
              onClose={handleClose}
              aria-labelledby="modal-modal-title"
              aria-describedby="modal-modal-description"
            >
              <Box sx={style}>
                <Typography id="modal-modal-title" variant="h6" component="h2">
                  Xác Nhận
                </Typography>
                <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                  Bạn xác nhận muốn xóa {brandname} có id là {idBrand} không ?
                </Typography>
                <Box sx={{ mt: 2, display: 'flex' }}>
          <Button sx={{color:'red'}} onClick={handleClose}>Đóng</Button>
          <Button onClick={handleDelete} >Tiếp tục</Button>
          </Box>
              </Box>
            </Modal>
          </div>
        
          <Box className="breadcrumb">
            <Breadcrumb routeSegments={[{ name: "Quản lý chi nhánh" }]} />
          </Box>
          <Box

            width="74vw"
          >

            <SimpleCard title="Chi Nhánh" setOpenModal={setOpenModal} >
              <StyledTable>
                <TableHead>
                  <TableRow>
                    <TableCell align="left"><b>STT</b></TableCell>
                    <TableCell align="left"><b>Chi nhánh</b></TableCell>
                    <TableCell align="left"><b>Địa Chỉ</b></TableCell>
                    <TableCell align="center"><b>Chỉnh Sửa</b></TableCell>
                    <TableCell align="center"><b>Xóa</b></TableCell>
                  </TableRow>
                </TableHead>
                {brand && brand ?
                  <TableBody>
                    {brand.map((brand, index) => (
                      <TableRow key={index}>
                        <TableCell align="left">{brand.id}</TableCell>
                        <TableCell align="left">{brand.branchName}</TableCell>
                        <TableCell align="left">{brand.address}</TableCell>
                        <TableCell align="center">
                          <IconButton onClick={() => handleEdit(brand.id)}>
                            <Icon color="primary" sx={{transform: 'translateY(-5px)'}} ><EditIcon /></Icon>
                          </IconButton>
                        </TableCell>
                        <TableCell align="center">
                          {/* <IconButton onClick={() => handleOnClick(brand.id)}>
                            <Icon color="error"><DeleteIcon /></Icon> */}
                             <IconButton onClick={() => handleOpen(brand.branchName,brand.id)}>
                            <Icon color="error" sx={{transform: 'translateY(-5px)'}}><DeleteIcon /></Icon>

                          </IconButton>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody> : <></>
                }

              </StyledTable>
            </SimpleCard>
          </Box>
        </>
      )}


    </Container>

  );
}
