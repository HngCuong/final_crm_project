import React from 'react'
import { useQuery } from '@tanstack/react-query';
import { useState, useEffect } from 'react';
import { formartDate, formatNumber } from '~/utils/functions';
import {
  Card,
  CardBody,
  CardHeader,
  CardFooter,
  Avatar,
  Typography,
  Tabs,
  TabsHeader,
  Tab,
  Switch,
  Tooltip,
  Button,
} from "@material-tailwind/react";
import {
  HomeIcon,
  ChatBubbleLeftEllipsisIcon,
  Cog6ToothIcon,
  PencilIcon,
} from "@heroicons/react/24/solid";
import { Link } from "react-router-dom";
import { ProfileInfoCard, MessageCard } from "~/components/Component/ProfileInfoCard";
import {
  ImageList,
  ImageListItem,
  Box,
  Paper, Grid, Divider,
  IconButton,

} from "@mui/material";
import img from '~/assets/profile.jpeg'
import { useParams } from 'react-router-dom';
import { Edit as EditIcon, PriorityHigh as PriorityHighIcon } from '@mui/icons-material';
import { useAxios } from '~/context/AxiosContex';
import './TicketDetail.css';
export default function StaffManager() {
  const { getTicketById, getCustomerById, getLogByTicketId } = useAxios();
  const [customerData, setCustomerData] = useState(null);
  const [customerDatas, setCustomerDatas] = useState(null);
  const [log, setLog] = useState([]);
  const { id } = useParams();
  const { idUser } = useParams();
  const {
    data: customers,
    isLoading,

  } =
    useQuery(
      {
        queryKey: ['customers', id],
        queryFn: async () => await getTicketById(id),
      }
    );

  const {
    data: customer,
    isLoading: loading
  } =
    useQuery(
      {
        queryKey: ['customer', idUser],
        queryFn: async () => await getCustomerById(idUser),
      }
    );
  useEffect(() => {
    if (customer) {
      setCustomerData(customer);
      console.log(customer);
    }
  }, [customer]);
  useEffect(() => {
    if (customers) {
      setCustomerDatas(customers);
      console.log(customers);
    }
  }, [customers]);

  function handleOnClick(id) {
    console.log('Co Nhan nha');
    setEditOpenModal(true);
    setId(id);
  }

  const {
    data: logticket,
  } =
    useQuery(
      {
        queryKey: ['logticket', id],
        queryFn: async () => await getLogByTicketId(id),
      }
    );
  useEffect(() => {
    if (logticket) {
      setLog(logticket);
      console.log(logticket);
    }
  }, [logticket]);

  const itemData = [
    {
      img: 'https://images.unsplash.com/photo-1551963831-b3b1ca40c98e',
      title: 'Breakfast',
    },
    {
      img: 'https://images.unsplash.com/photo-1551782450-a2132b4ba21d',
      title: 'Burger',
    },
    {
      img: 'https://images.unsplash.com/photo-1522770179533-24471fcdba45',
      title: 'Camera',
    }

  ];
  return (
    <>
      {(customerData && customerDatas && log) ? (
        <div className='block w-[77.9vw]'>
          <div className="relative mt-8 h-20 w-full overflow-hidden rounded-xl bg-[url('/img/background-image.png')] bg-cover	bg-center">
            <div className="absolute inset-0 h-full w-full bg-gray-900/75" />
          </div>
          <Card className="mx-3 -mt-10 mb-6 lg:mx-4 border border-blue-gray-100">
            <CardBody className="p-4 w-full">
              <div className="mb-10 flex items-center justify-between flex-wrap gap-6">
                <div className="flex items-center gap-6">
                  <Avatar
                    src={img}
                    alt="bruce-mars"
                    size="sm"
                    variant="rounded"
                    className="rounded-lg shadow-lg shadow-blue-gray-500/40"
                    style={{ width: '50px', height: '50px', objectFit: 'cover' }}
                  />
                  <div>
                    <Typography variant="h5" color="blue-gray" className="mb-1">
                      {customerData.name}
                    </Typography>
                    <Typography
                      variant="small"
                      className="font-normal text-blue-gray-600"
                    >
                      Hoạt Động
                    </Typography>
                  </div>
                </div>
                <div className="w-96">
                  <Tabs value="app">
                    <TabsHeader>
                      <Tab value="app">
                        <HomeIcon className="-mt-1 mr-2 inline-block h-5 w-5" />
                        Home
                      </Tab>
                      <Tab value="message">
                        <ChatBubbleLeftEllipsisIcon className="-mt-0.5 mr-2 inline-block h-5 w-5" />
                        Message
                      </Tab>
                      <Tab value="settings">
                        <Cog6ToothIcon className="-mt-1 mr-2 inline-block h-5 w-5" />
                        Settings
                      </Tab>
                    </TabsHeader>
                  </Tabs>
                </div>
              </div>
              <div className="gird-cols-1 mb-12 grid gap-12 px-4 lg:grid-cols-2 xl:grid-cols-3 justify-end items-end">
                <div className='h-full'>
                  <Typography variant="h6" color="blue-gray" className="mb-3">
                    Thông Tin Ticket
                  </Typography>
                  <div className="flex flex-col gap-1">
                    <Typography className="mb-1 block text-xs font-semibold uppercase text-blue-gray-500 w-full">
                      Tags
                    </Typography>
                    <Typography
                      variant="small"
                      className="font-normal text-blue-gray-500  w-full"
                    >
                      {customerDatas.tags}
                    </Typography>
                    <Typography className="mb-1 block text-xs font-semibold uppercase text-blue-gray-500 w-full">
                      Tiêu Đề
                    </Typography>
                    <Typography
                      variant="small"
                      className="font-normal text-blue-gray-500  w-full"
                    >
                      {customerDatas.title}
                    </Typography>
                    <Typography className="mb-1 block text-xs font-semibold uppercase text-blue-gray-500 w-full">
                      Ngày Tạo
                    </Typography>
                    <Typography
                      variant="small"
                      className="font-normal text-blue-gray-500  w-full"
                    >
                    {formartDate(customerDatas.createdDate, 'full')}
                    </Typography>
                    <Typography className="mb-1 block text-xs font-semibold uppercase text-blue-gray-500 w-full">
                      Trạng Thái
                    </Typography>
                    <Typography
                      variant="small"
                      className="font-normal text-blue-gray-500  w-full"
                    >
                      {customerDatas.ticketStatusId}
                    </Typography>
                    <Typography className="mb-1 block text-xs font-semibold uppercase text-blue-gray-500 w-full">
                      Nội Dung
                    </Typography>
                    <Typography
                      variant="small"
                      className="font-normal text-blue-gray-500  w-full"
                    >
                      {customerDatas.note}
                    </Typography>
                    <Typography className="mb-1 block text-xs font-semibold uppercase text-blue-gray-500 w-full">
                      Ưu Tiên
                    </Typography>
                    <Typography
                      variant="small"
                      className="font-normal text-blue-gray-500  w-full"
                    >
                      {customerDatas.levelId}
                    </Typography>
                    <Typography className="mb-1 block text-xs font-semibold uppercase text-blue-gray-500 w-full">
                      Loại
                    </Typography>
                    <Typography
                      variant="small"
                      className="font-normal text-blue-gray-500 w-full overflow-wrap break-words"

                    >
                      {customerDatas.ticketTypeId}
                    </Typography>

                  </div>
                </div>
                <div className='h-full'>
                  <ProfileInfoCard
                    title="Thông tin Hợp Đồng"
                    description="Hợp đồng đăng ký sử dụng nước hộ dân là một bước quan trọng trong việc đảm bảo cung cấp nước sạch và an toàn cho cộng đồng. Bằng việc ký kết hợp đồng này, cả hai bên - nhà cung cấp nước và người sử dụng - cam kết tuân thủ các điều khoản về việc cung cấp, sử dụng và thanh toán cho dịch vụ nước."
                    details={{
                      "fullname": customerData.lastName + " " + customerData.firstName,
                      mobile: customerData.phoneNumber,
                      email: customerData.email,
                      location: customerData.address,
                      social: (
                        <div className="flex items-center gap-4">
                          <i className="fa-brands fa-facebook text-blue-700" />
                          <i className="fa-brands fa-twitter text-blue-400" />
                          <i className="fa-brands fa-instagram text-purple-500" />
                        </div>
                      ),
                    }}
                    action={
                      <Tooltip content="Edit Profile">
                        <PencilIcon className="h-4 w-4 cursor-pointer text-blue-gray-500" />
                      </Tooltip>
                    }
                  />
                </div>
                <div className='h-full'>

                  <Typography variant="h6" color="blue-gray" className="mb-3">
                    Báo Cáo
                  </Typography>
                  {
                   log.length > 0 && log.map((tag, index) => (
                      <div>
                        <ul className="flex flex-col gap-6">
                          <MessageCard

                            message={tag.note}
                            name={tag.createdDate}
                            action={
                              <Button variant="text" size="sm">
                                reply
                              </Button>
                            }
                          />
                        </ul>
                      </div>
                    ))}


                </div>
              </div>

            </CardBody>
            <ImageList sx={{ width: 900, height: 400 }} cols={3} rowHeight={164}>
              {log.length > 0 && log.map((item) => (
                item.imgUrl != null ? (
                  <ImageListItem key={item.imgUrl}>
                    <img
                      srcSet={`${item.imgUrl}?w=164&h=164&fit=crop&auto=format&dpr=2 2x`}
                      src={`${item.imgUrl}?w=164&h=164&fit=crop&auto=format`}
                      alt={item.imgUrl}
                      loading="lazy"
                    />
                  </ImageListItem>
                ) : null // Trả về null nếu không có imgUrl
              ))}
            </ImageList>
          </Card>

        </div>
      ) : null}
    </>


  );
}
