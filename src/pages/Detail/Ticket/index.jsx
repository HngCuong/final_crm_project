
import {
  Card,
  Input,
  Checkbox,
  Button,
  Typography,
} from "@material-tailwind/react";
// modules
import { useQuery, useMutation } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';
import { useEffect, useMemo, useState, useRef, useCallback } from 'react';
// context
import { useAxios } from '~/context/AxiosContex';
import { useAuth } from '~/context/AuthContext';
//functions
import { formatMoney } from '~/utils/functions';
import { toast } from 'react-hot-toast';
import { useNavigate } from 'react-router-dom';
import {
 
  Box,
  Grid,
  styled,
  Autocomplete
} from "@mui/material";
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { Span } from "~/components/Component/Typography/Typography";
import { format } from 'date-fns';
import HomeIcon from '@mui/icons-material/Home';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import img from '~/assets/customer-service.png'
const TextField = styled(TextValidator)(() => ({
  width: "100%",
  marginBottom: "16px"
}));


const Ticket = () => {
  const [customerData, setCustomerData] = useState('');
  const {user} = useAuth();
  const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
  const checkedIcon = <CheckBoxIcon fontSize="small" />;
  const navigate = useNavigate();
  const { postTicket, getTicketType, getLevel,postMail,getCustomerById } =
    useAxios();
  const { id } = useParams();
  const { idUser } = useParams();
  const { idCon } = useParams();
  const note = useRef();
  const ticketTypeId = useRef();
  const title = useRef();
  const levelId = useRef();
  const {
    data: customer,
    isLoading: loading
  } =
    useQuery(
      {
        queryKey: ['customer', idUser],
        queryFn: async () => await getCustomerById(idUser),
      }
    );
  useEffect(() => {
    if (customer) {
      setCustomerData(customer);
      console.log(customer);
    }
  }, [customer]);
  const {
    data: type,
  } = useQuery(
    {
      queryKey: ['type'],
      queryFn: async () => await getTicketType(),
    }
  );
  const {
    data: level,
  } = useQuery(
    {
      queryKey: ['level'],
      queryFn: async () => await getLevel(),
    }
  );
  //* Mutatuion tạo khách hàng
  const postTicketMutation = useMutation({
    mutationFn: (data) => {
      console.log(data);
      return postTicket(data).then((res) => {
        return res;
      });
    },
    onSuccess(data) {
      handlePostMail();
      console.log("Da chay goi mail");
      if (data == '') {
        handlePostMail();
        toast('Tạo thành công Ticket', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });

      } else {
        toast('Có lỗi trong quá trình tạo Ticker', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
      }
    },
  });
  //* Hàm tạo Ticket
  const handleAddTicket = (e) => {
    e.preventDefault();
    if (
      !note.current.value ||
      !ticketTypeId.current.value ||
      !levelId.current.value
    ) {
      toast('Không được bỏ trống', {
        icon: '👏',
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      });
      return;
    }
    const data = {
      // callId: idCon,
      // note: note.current?.value,
      // customerId: idUser,
      // ticketTypeId: ticketTypeId.current?.value,
      // levelId: levelId.current?.value,
      // ticketStatusId: 1,
      // createdBy: format(Date.now(), 'yyyy-MM-dd')
      branchId: parseInt(Object.values(user)[4]),
      note: note.current?.value,
      customerId: parseInt(idUser),
      ticketTypeId: parseInt(ticketTypeId.current?.value),
      levelId: parseInt(levelId.current?.value),
      ticketStatusId: 1,
      createdDate: format(Date.now(), 'yyyy-MM-dd HH:mm:ss.SSS'),
      createdBy: 'staff',
      assignedUserId: 1,
      code: format(Date.now(), 'yyyy-MM-dd HH:ss:ss.SSS'),
      title: title.current?.value
    };
    postTicketMutation.mutate({
      data
    });
  };
  const top100Films = [
    { title: 'Sự cố'},
    { title: 'Xử Lý' },
    { title: 'Hỏng Hóc' },
    { title: 'Hỗ Trợ' },
  ];
  const postMailMutation = useMutation({
    mutationFn: (data) => {
      return postMail(data).then((res) => {
        console.log(res);
        return res;
      });
    },
  });
  //* Hàm tạo Ticket
  const handlePostMail = (e) => {
  const data = {
    subject: "Tạo Ticket Thành Công",
    body: "Chúng tôi đã gửi yêu cầu của bạn đi xử lý. Chúng tôi xe gửi mail để cập nhật tiến độ",
    toAddresses:[customerData.email]
  };
  postMailMutation.mutate({
    data
  });
};
  return (
    <Box
      width="100%"
      justifyContent="center"
      alignItems="center"
      sx={{
        backgroundColor: 'white',
        '& > *': {
          margin: '0px', // Apply margin of 10 units to direct children
        },
      }}
    >
      <Typography style={{ fontFamily: 'Arial, sans-serif', fontSize: '20px', fontWeight: 'bold', color: '#1E0342' }}>
      <HomeIcon/>  <ArrowForwardIosIcon/>Tạo Ticket
          </Typography>
      <Box>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          
        </div>
      </Box>
      <ValidatorForm onSubmit={handleAddTicket} onError={() => null}>
        <Grid container spacing={6}>
          <Grid item lg={4} md={4} sm={12} xs={12} sx={{ mt: 1 }}>
          {/* <Typography variant="h6" color="blue-gray" className="mb-3">
            Mã Hợp Đồng
          </Typography>
          <Input
            value={idCon}
            size="lg"
            placeholder="HĐ2191"
            className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
            labelProps={{
              className: "before:content-none after:content-none",
            }}
          /> */}
            {/* <TextField
              type="text"
              name="Call Code"
              id="standard-basic"
              label="Call Code"
              value={idCon}
            /> */}
             <Typography variant="h6" color="blue-gray" className="mb-3">
            Số Khách Hàng
          </Typography>
          <Input
            value={customerData.phoneNumber}
            size="lg"
            placeholder="0776190244"
            className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
            labelProps={{
              className: "before:content-none after:content-none",
            }}
          />
            {/* <TextField
              type="text"
              name="Customer"
              label="Customer"
              value={idUser}
            /> */}
            <Typography variant="h6" color="blue-gray" className="mt-3 mb-2">
            Tiêu Đề
          </Typography>
          <Input
            fullWidth
            type="text"
            inputRef={title}
            size="lg"
            placeholder="Tiêu đề"
            className=" !border-t-blue-gray-200 focus:!border-t-gray-900 "
            labelProps={{
              className: "before:content-none after:content-none",
            }}
          />
             <Typography variant="h6" color="blue-gray" className="mt-3 mb-2">
            Nội Dung
          </Typography>
          <Input
            multiline
            fullWidth
            type="text"
            rows={3}
            inputRef={note}
            size="lg"
            placeholder="Nhà bị rò rĩ nước cần xử lý gấp"
            className=" !border-t-blue-gray-200 focus:!border-t-gray-900"
            labelProps={{
              className: "before:content-none after:content-none",
            }}
          />
            {/* <TextField
              type="text"
              name="Type"
              label="Note"
              multiline
              rows={5}
              inputRef={note}
              fullWidth  // Mở rộng ô text để lấp đầy toàn bộ chiều rộng của parent
            /> */}

          </Grid>

          <Grid item lg={4} md={4} sm={12} xs={12} sx={{ mt: 1 }}>
            {/* <TextField
              type="text"
              name="Level"
              id="standard-basic"
              inputRef={levelId}
              label="Level"
           
            /> */}
            <Box>
            <Typography variant="h6" color="blue-gray" className="mb-3">
            Chọn Cấp Độ
          </Typography>
             
              <div>
                {/* onChange={(e) => setsourceId(e.target.value)} value={xsourceId} */}
                <select style={{ width: '100%', height: '6.5vh' }} className="border border-t-blue-gray-200" ref={levelId}>
                  {level &&
                    level.map((lv, index) => {
                      if (lv?.levelName) {
                        return (
                          <option
                            key={index}
                            value={lv.id}
                          >
                            {lv.levelName}
                          </option>
                        );
                      }
                    })}
                </select>
              </div>
            </Box>

            <Box sx={{}}>
            <Typography variant="h6" color="blue-gray" className="mb-3">
            Chọn Loại
          </Typography>
              <div>
                {/* onChange={(e) => setsourceId(e.target.value)} value={xsourceId} */}
                <select style={{ width: '100%', height: '6.5vh' }} className="border border-t-blue-gray-200 " ref={ticketTypeId}>
                  {type &&
                    type.map((lv, index) => {
                      if (lv?.ticketTypeName) {
                        return (
                          <option
                            key={index}
                            value={lv.id}
                          >
                            {lv.ticketTypeName}
                          </option>
                        );
                      }
                    })}
                </select>
              </div>
            </Box>

            <Box style={{ }}>
            <Typography variant="h6" color="blue-gray" className="mb-3">
            Nhãn
          </Typography>
              <Autocomplete
                multiple
                id="checkboxes-tags-demo"
                options={top100Films}
                disableCloseOnSelect
                getOptionLabel={(option) => option.title}
                renderOption={(props, option, { selected }) => (
                  <li {...props}>
                    <Checkbox
                      icon={icon}
                      checkedIcon={checkedIcon}
                      style={{ marginRight: 8 }}
                      checked={selected}
                    />
                    {option.title}
                  </li>
                )}
                style={{ width: 275 }}
                renderInput={(params) => (
                  <TextField {...params} label="Tags"  />
                )}
              />
            </Box>
          </Grid>
          <Grid item lg={4} md={4} sm={12} xs={12} sx={{ mt: 2 }}>
          <Box
      sx={{
        width: '100%', // 100% chiều rộng của viewport
        display: 'block',
      }}
    >
      <div className="items-center justify-center w-full">
      <img src={img} alt="Description of the image" style={{ maxHeight: '400px', maxWidth: '18vw',marginLeft:'50px',marginBottom:'20px' }} />
      </div>
      {/* <div className="c" style={{ fontSize: "16px",marginLeft:'80px',marginBottom:'5px',fontWeight:'bold' }}>Tên Nhân Viên: Cường</div>
<div className="c" style={{ fontSize: "16px",marginLeft:'80px',marginBottom:'5px',fontWeight:'bold' }}>Vai Trò: Nhân Viên</div>
<div className="c" style={{ fontSize: "16px",marginLeft:'80px',fontWeight:'bold' }}>Chúc Vụ: Tổng Đài</div> */}

    </Box>
          </Grid>

        </Grid>
        <Button color="primary" variant="contained" type="submit" className="mt-6" >
          <Span sx={{ pl: 1, textTransform: "capitalize" }}>Submit</Span>
        </Button>
      </ValidatorForm>
    </Box>
  );
};

export default Ticket;
